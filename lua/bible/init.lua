local curl = require('plenary.curl')

function LookupBibleVerse(ref)

    -- TODO: get ref from "highlighted" word in visual mode?
    ref = ref or vim.fn.input("Verse? ")

    -- Make the API request
    local response = curl.get("https://api.esv.org/v3/passage/text", {
        query = {
            q = ref,
            ["include-passage-references"] = "false",
            ["include-verse-numbers"] = "false",
            ["include-footnotes"] = "false",
            ["include-headings"] = "false",
            ["include-short-copyright"] = "false",
        },

        headers = {
            Authorization = "Token a0e122c0b20aee5601d9a6ea50e16194f5131eda",
        },
    })

    -- check for the data we need
    --if not result or not result.passages or #result.passages == 0 then
        --return
    --end

    -- Parse the JSON response
    local result = vim.fn.json_decode(response.body)
    -- Extract the Bible verse text
    local verse = string.gsub(result.passages[1], "\n", "")
    -- Insert the Bible verse before the selected text
    vim.fn.append(vim.fn.getcurpos()[2], verse)
end

-- Add the keymap for Bible verse lookup
vim.keymap.set({ 'n', 'v' }, '<leader>bv', '<cmd>lua LookupBibleVerse()<CR>')


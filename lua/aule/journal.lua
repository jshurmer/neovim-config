function Journaling()
    vim.cmd([[
        iabbrev primary ⍟
        iabbrev collab ⁂

        iabbrev done ✓
        iabbrev todo ○
        iabbrev event 📅
        iabbrev priority ❗


        syntax match JournalAll /.*/
        syntax match JournalHeading /^\(week\|tickets\|thoughts\).*/
        syntax match JournalTodo /^\s*○.*/
        syntax match JournalDone /^\s*✓.*/
        syntax match JournalCancel /^\s*x.*/
        syntax match JournalPrimary /^\s*⍟.*/
        syntax match JournalCollab /^\s*⁂.*/
        syntax match JournalEvent /^\s*📅.*/
        syntax match JournalPriority /^\s*❗.*/

        highlight JournalAll ctermfg=Grey guifg=LightGrey
        highlight JournalHeading ctermfg=White cterm=underline,bold guifg=White gui=underline,bold
        highlight JournalPriority ctermfg=Green ctermbg=NONE guifg=SpringGreen gui=bold,italic
        highlight JournalTodo ctermfg=Green ctermbg=NONE guifg=SpringGreen
        highlight JournalDone ctermfg=Grey ctermbg=NONE guifg=Grey
        highlight JournalCancel ctermfg=Grey cterm=strikethrough guifg=#51504f gui=strikethrough
        highlight JournalPrimary ctermfg=Yellow ctermbg=NONE guifg=Yellow
        highlight JournalCollab ctermfg=Cyan ctermbg=NONE guifg=Cyan
        highlight JournalEvent ctermfg=Magenta ctermbg=NONE guifg=MediumOrchid

    ]])
end


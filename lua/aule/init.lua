-- put this at the top so any plugins utilizing it will get the correct one
vim.g.mapleader = " "
vim.g.maplocalleader = " "

require("aule.journal")
require("aule.lazy")
require("aule.bible")


--a lot of these came from The Primeagen
--https://github.com/ThePrimeagen/init.lua
--and from the LazyVim stuff
--https://github.com/LazyVim/LazyVim/tree/main/lua/lazyvim
--

--set options
vim.opt.nu = true
vim.opt.relativenumber = true
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.smartindent = true
vim.opt.wrap = false
vim.opt.termguicolors = true
vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")
vim.opt.updatetime = 50 --very fast autosave
vim.opt.colorcolumn = "90"

-- live search highlighting!
vim.opt.hlsearch = false
vim.opt.incsearch = true

--customize the file browser
vim.g.netrw_keepdir = 0
vim.g.netrw_winsize = 33
vim.g.netrw_liststyle = 3
vim.g.netrw_sizestyle = "H"
vim.cmd('highlight netrwMarkFile guifg=75')

--use undo tree instead of swap files
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

--   _                                 
--  | |_____ _  _   _ __  __ _ _ __ ___
--  | / / -_) || | | '  \/ _` | '_ (_-<
--  |_\_\___|\_, | |_|_|_\__,_| .__/__/
--          |__/             |_|      
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- file tree stuff (using netrw ootb)
-- some from https://vonheikemen.github.io/devlog/tools/using-netrw-vim-builtin-file-explorer/
-- some in the docs https://neovim.io/doc/user/pi_netrw.html
vim.keymap.set("n", "<leader>pv", vim.cmd.Lexplore) --toggle netrw in a pane to the left

-- pasting over a highlighted word will keep the previous copied thing copied
vim.keymap.set("x", "<leader>p", [["_dP]])

-- copy into the system clipboard
vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]])
vim.keymap.set("n", "<leader>Y", [["+Y]])

vim.keymap.set({ "n", "v" }, "<leader>d", [["_d]])
vim.keymap.set("n", "Q", "<nop>")

vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

vim.keymap.set("n", "<leader><leader>", function()
	vim.cmd("so")
end)


local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    spec = {
        {
            "tomasiser/vim-code-dark",
            lazy = false,
            priority = 1000, -- primary color scheme needs this priority so it loads blazingly fast
            config = function()
                -- extra color customization
                vim.g.codedark_modern = 1
                vim.g.codedark_italics = 1
                vim.cmd([[colorscheme codedark]])
            end,
        },

        --todo: folke/whick-key.nvim

        -- telescope does Fuzy Finding stuf
        {
            "nvim-telescope/telescope.nvim",
            lazy = false,
            priority = 1000,
            branch = "0.1.x",
            dependencies = {
                "nvim-lua/plenary.nvim"
            },
            keys = {
                { "<leader>ff", "<cmd>Telescope find_files<cr>" },
                { "<leader>fg", "<cmd>Telescope git_files<cr>" },
                { "<leader>f*", "<cmd>Telescope live_grep<cr>" },
                { "<leader>fa", "<cmd>Telescope commands<cr>" },
                { "<leader>fq", "<cmd>Telescope command_history<cr>" },
            },
        },

        -- mini mufremove helps manage buffers a bit. 
        -- todo: is this needed?
        {
            "echasnovski/mini.bufremove",
            lazy = false,
            -- stylua: ignore
            keys = {
                { "<leader>bd", function() require("mini.bufremove").delete(0, false) end, desc = "Delete Buffer" },
                { "<leader>bD", function() require("mini.bufremove").delete(0, true) end, desc = "Delete Buffer (Force)" },
            },
        },

        -- add diagnostics, errors, etc
        {
            "folke/trouble.nvim",
            dependencies = { "nvim-tree/nvim-web-devicons" },
        },


        -- treesitter for good ast parsing stuff
        {
            "nvim-treesitter/nvim-treesitter",
            build = ":TSUpdate",
            cmd = { "TSUpdateSync" },
            event = { "BufReadPost", "BufNewFile" },
            opts = {
                --turn on highlighting and indent control
                highlight = { enable = true },
                indent = { enable = true },
                ensure_installed = {
                    "comment",
                    "bash",
                    "html",
                    "javascript",
                    "jsdoc",
                    "css",
                    "svelte",
                    "scss",
                    "lua",
                    "luadoc",
                    "luap",
                    "markdown",
                    --"mardkown_inline",
                    "query",
                    "regex",
                    "vim",
                    "vimdoc",
                    "yaml",
                    "json",
                    "toml",
                    "java",
                    "diff",
                    "dockerfile",
                    "git_config",
                    "git_rebase",
                    "gitattributes",
                    "gitcommit",
                    "gitignore",
                    "http",
                    "make",
                    "rust",
                },
            },
            config = function(_, opts)
                require("nvim-treesitter.configs").setup(opts)
            end,
            --note: more configuration available, e.g. "incremental selection"
            --see https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/plugins/treesitter.lua
        },

        -- undotree for good local history
        {
            "mbbill/undotree",
            keys = {
                { "<leader>u", vim.cmd.UndotreeToggle },
            },
        },

        -- fugitive for :Git commands
        {
            "tpope/vim-fugitive",
            cmd = { "Git" },
        },

        -- typing practice
        {
            "BooleanCube/keylab.nvim",
            cmd = { "KeylabStart" },
            keys = {
                { "<leader>kl", vim.cmd.KeylabStart },
                { "<leader>ks", vim.cmd.KeylabStop },
            },
        },

        -- neorg - journaling etc
        { 
            "nvim-neorg/neorg",
            build = ":Neorg sync-parsers",
            dependencies = { "nvim-lua/plenary.nvim" },
            cmd = { "Neorg" },
            opts = {
                load = {
                    ["core.defaults"] = {},
                    ["core.concealer"] = {},
                    ["core.dirman"] = {
                        config = {
                            workspaces = {
                                wbd = "~/journal/wbd",
                                personal = "~/journal/personal",
                            },
                            default_workspace = "personal",
                        },
                    },
                },
            },
        },
    },



    checker = { enabled = true }, -- automatically check for plugin updates
    performance = {
        rtp = {
            -- disable some rtp plugins
            disabled_plugins = {
                "gzip",
                -- "matchit",
                -- "matchparen",
                -- "netrwPlugin",
                "tarPlugin",
                "tohtml",
                "tutor",
                "zipPlugin",
            },
        },
    },
})

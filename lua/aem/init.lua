
local aem_env

vim.keymap.set("n", "<leader>aem", function()
    aem_env = aem_env or vim.fn.input("Env? ")
    vim.cmd("w")
    vim.cmd("! post-to-sling -a " .. aem_env .. " %:p")
end)

vim.keymap.set("n", "<leader>pem", function()
    aem_env = aem_env or vim.fn.input("Env? ")
    vim.cmd("w")
    vim.cmd("! post-to-sling -ap " .. aem_env .. " %:p")
end)

vim.keymap.set("n", "<leader>aemf", function()
    aem_env = aem_env or vim.fn.input("Env? ")
    vim.cmd("w")
    vim.cmd("! post-to-sling -af " .. aem_env .. " %:p")
end)

vim.keymap.set("n", "<leader>pemf", function()
    aem_env = aem_env or vim.fn.input("Env? ")
    vim.cmd("w")
    vim.cmd("! post-to-sling -apf " .. aem_env .. " %:p")
end)

vim.keymap.set("n", "<leader>lem", function()
    vim.cmd("w")
    vim.cmd("! post-to-sling -l local" .. " %:p")
end)

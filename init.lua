vim.lsp.set_log_level("off")



-- put this at the top so any plugins utilizing it will get the correct one
vim.g.mapleader = " "
vim.g.maplocalleader = " "

--a lot of these came from The Primeagen https://github.com/ThePrimeagen/init.lua
--and from the LazyVim stuff https://github.com/LazyVim/LazyVim/tree/main/lua/lazyvim

-- OPTIONS
--=========--
vim.opt.nu = true -- line numbers
vim.opt.relativenumber = true
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.smartindent = true
vim.opt.breakindent = true
vim.opt.wrap = false
vim.opt.termguicolors = true
vim.opt.scrolloff = 8 -- keep some realestate at the edges of the viewport
vim.opt.signcolumn = "yes"
vim.opt.updatetime = 50 --very fast autosave
vim.opt.colorcolumn = "90"

-- mouse support on
vim.opt.mouse = "a"

-- live search highlighting!
vim.opt.incsearch = true
vim.opt.inccommand = "split"
vim.opt.hlsearch = true
vim.keymap.set('n', '<Esc>', [[<cmd>nohlsearch<CR>]])

-- use persistent undos instead of swap files (undoTree is added below as well)
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- FILE BROWSER
-- some from https://vonheikemen.github.io/devlog/tools/using-netrw-vim-builtin-file-explorer/
-- some in the docs https://neovim.io/doc/user/pi_netrw.html
--==============--
vim.g.netrw_winsize = 33
vim.g.netrw_liststyle = 0 -- list view
vim.g.netrw_sizestyle = "H" -- human readable size
vim.cmd('highlight netrwMarkFile guifg=75') -- imrpove some colors a bit

--   _                                 
--  | |_____ _  _   _ __  __ _ _ __ ___
--  | / / -_) || | | '  \/ _` | '_ (_-<
--  |_\_\___|\_, | |_|_|_\__,_| .__/__/
--          |__/             |_|      
-- KEYMAPS
--=========--
vim.keymap.set("n", "<leader>bb", [[:! build.sh -b]])
vim.keymap.set("n", "<leader>bl", [[:! build.sh -l]])
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv") --move line down in visual mode
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv") --move line up in visual mode

-- Keep the viewport centered when moving up/down/searching
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")
vim.keymap.set("n", "J", "mzJ`z") -- keep cursor in place when using shift-J


vim.keymap.set("x", "<leader>p", [["_dP]]) -- pasting over a word does not yank it

vim.keymap.set("n", "<leader>r", vim.cmd.LspRestart)

--re-indent the file, return the cursor to where it was
vim.keymap.set("n", "<leader>ai", [[mzgg=G`z]])

-- copy into the system clipboard
vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]])
vim.keymap.set({ "n", "v" }, "<leader>Y", [["+Y]])
vim.keymap.set({ "n", "v" }, "<leader>d", [["+d]])
vim.keymap.set({ "n", "v" }, "<leader>D", [["+D]])
vim.keymap.set({ "n", "v" }, "<leader>p", [["+p]])
vim.keymap.set({ "n", "v" }, "<leader>P", [["+P]])

vim.keymap.set({ "n", "v" }, "<leader>d", [["_d]])
vim.keymap.set("n", "Q", "<nop>")

vim.keymap.set("n", "<leader>l", vim.cmd.Lazy)

-- Replace word in file
vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

-- source a file
vim.keymap.set("n", "<leader><leader>", function()
    vim.cmd("so")
end)

-- lsp autosuggestions
vim.keymap.set({ "n", "i" }, "<C-a>", "<cmd>lua vim.lsp.buf.code_action()<CR>")

-- Inspect the tresitter attributes of the thing under the cursor
vim.keymap.set("n", "<leader>i", vim.cmd.Inspect)

Ds_Client = nil
vim.keymap.set("n", "<leader>ds", function()
    if Ds_Client == nil then
        Ds_Client = vim.lsp.start({
            name = 'ds-lsp',
            cmd = { '/Users/jshurmer/dds/sni-foundation/sni-foundation-clientlibs/lsp/start.sh' },
            root_dir = vim.fs.root(0, { 'pom.xml', '.git' }),
        })
    else
        vim.lsp.stop_client(Ds_Client)
        Ds_Client = nil
    end
end, { desc = "extra DS stuff. Load LSP" })

-- COMMANDS
--=========--

vim.api.nvim_create_user_command('Two', function ()
    vim.opt.tabstop = 2
    vim.opt.softtabstop = 2
    vim.opt.shiftwidth = 2
end , {})


-- JOURNALING
--============--

-- enable Journaling
vim.keymap.set("n", "<leader>J", "<cmd>lua Journaling()<CR>")
function Journaling()
    vim.opt.wrap = true
    vim.opt.textwidth = 80
    --vim.cmd([[
    --iabbrev primary ⍟
    --iabbrev collab ⁂

    --iabbrev attention ⚠️
    --iabbrev done ✓
    --iabbrev todo ○
    --iabbrev event 📅
    --iabbrev priority ❗

    --]])

    --    vim.cmd([[
    --    syntax match JournalAll /.*/
    --    syntax match JournalHeading /^\(week\|tickets\|thoughts\).*/
    --    syntax match JournalSubHead /^\(UP FRONT\|REVIEW\).*/
    --    syntax match JournalTodo /^\s*○.*/
    --    syntax match JournalDone /^\s*✓.*/
    --    syntax match JournalCancel /^\s*x.*/
    --    syntax match JournalPrimary /^\s*⍟.*/
    --    syntax match JournalCollab /^\s*⁂.*/
    --    syntax match JournalEvent /^\s*📅.*/
    --    syntax match JournalPriority /^\s*❗.*/
    --    syntax match JournalAttention /^\s*⚠️.*/
    --
    --    highlight JournalAll ctermfg=Grey guifg=LightGrey
    --    highlight JournalAttention ctermfg=Green cterm=bold,italic,standout guifg=SpringGreen gui=bold,italic,standout
    --    highlight JournalHeading ctermfg=White cterm=underline,bold guifg=White gui=underline,bold
    --    highlight JournalSubHead ctermfg=White cterm=bold,italic guifg=White gui=italic,bold
    --    highlight JournalPriority ctermfg=Green ctermbg=NONE guifg=SpringGreen gui=bold,italic
    --    highlight JournalTodo ctermfg=Green ctermbg=NONE guifg=SpringGreen
    --    highlight JournalDone ctermfg=Grey ctermbg=NONE guifg=Grey
    --    highlight JournalCancel ctermfg=Grey cterm=strikethrough guifg=#51504f gui=strikethrough
    --    highlight JournalPrimary ctermfg=Yellow ctermbg=NONE guifg=Yellow
    --    highlight JournalCollab ctermfg=Cyan ctermbg=NONE guifg=Cyan
    --    highlight JournalEvent ctermfg=Magenta ctermbg=NONE guifg=MediumOrchid
    --    ]])
end


-- PLUGINS
--=========--

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
    vim.fn.system({ "git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable", lazypath, })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup(
    {
        "rktjmp/lush.nvim",
        {
            "yorickpeterse/vim-paper",
            enabled = false,
            init = function () vim.cmd([[colorscheme paper]]) end
        },
        {
            "yorickpeterse/happy_hacking.vim",
            enabled = true,
            init = function () vim.cmd([[colorscheme happy_hacking]]) end
        },
        {
            "ntk148v/habamax.nvim",
            enabled = false,
            init = function ()   end
        },
        {
            "rebelot/kanagawa.nvim",
            enabled = false,
            opts = {},
            init = function () vim.cmd([[colorscheme kanagawa]])  end
        },
        {
            "AlexvZyl/nordic.nvim",
            enable = false,
            opts = {},
            init = function ()  end
        },
        {
            "okaihe/okai",
            enabled = false ,
            opts = {},
            init = function () vim.cmd([[colorschem okai]]) end
        },
        {
            "ayu-theme/ayu-vim",
            enabled = false,
            --opts = {},
            init = function () vim.cmd([[colorscheme ayu]])  end
        },
        {
            "rose-pine/neovim",
            name = "rose-pine",
            enabled = false,
            opts = {
                highlight_groups = {
                    TelescopeBorder = { fg = "overlay", bg = "overlay" },
                    TelescopeNormal = { fg = "subtle", bg = "overlay" },
                    TelescopeSelection = { fg = "text", bg = "highlight_med" },
                    TelescopeSelectionCaret = { fg = "love", bg = "highlight_med" },
                    TelescopeMultiSelection = { fg = "text", bg = "highlight_high" },

                    TelescopeTitle = { fg = "base", bg = "gold" },
                    TelescopePromptTitle = { fg = "base", bg = "foam" },
                    TelescopePreviewTitle = { fg = "base", bg = "iris" },

                    TelescopePromptNormal = { fg = "text", bg = "surface" },
                    TelescopePromptBorder = { fg = "surface", bg = "surface" },
                },
            },
            --init = function () vim.cmd([[colorscheme rose-pine]]) end
        },
        {
            "catppuccin/nvim",
            enabled = false,
            name = "catppuccin",
            priority = 1000,
            opts = {
                flavor = 'mocha',
                styles = {
                    comments = { "italic" },
                    conditionals = {  },
                },
                color_overrides = {
                    mocha = {
                        base = "#000000",
                        mantle = "#000000",
                        crust = "#000000",
                    },
                },
                custom_highlights = function(colors)
                    return {
                        ["@namespace"] = { style = {}},
                        ["@tag.attribute"] = { style = {}},
                        ["@text.uri"] = { style = {}},
                        ["@keyword.return"] = { style = { "italic", "bold", "underline" }},
                    }
                end,
                inegrations = {
                    mason = true,
                    --which_key = true,
                },
            },
            init = function() --[[vim.cmd.colorscheme 'catppuccin']] end,
        },

        -- helper tools like curl 
        { "nvim-lua/plenary.nvim" },

        {
            "zbirenbaum/copilot.lua",
            cmd = "Copilot",
            keys = {
                { "<c-a>g", "<cmd>Copilot<cr>" },
            },
            opts = {
                suggestion = {
                    keymap = {
                        accept = "<c-a><cr>",
                        accept_word = "<c-a>w",
                        accept_line = "<c-a>j",
                        next = "<c-a>a",
                        prev = "<c-a>s",
                        dismiss = "<c-a>z",
                    },
                },
            },
        },

        -- navigate between vim and tmux panes with the same keys
        {
            'christoomey/vim-tmux-navigator',
            event = "VeryLazy",
        },

        {
            "cbochs/grapple.nvim",
            cmd = "Grapple",
            opts = {
                scope = "git",
            },
            keys = {
                { "<leader>ha", "<cmd>Grapple toggle<cr>",         desc = "tag a file" },
                { "<leader>hh", "<cmd>Grapple toggle_tags<cr>",    desc = "toggle tags ui" },
                { "<leader>hH", "<cmd>Grapple toggle_scopes<cr>",  desc = "toggle tags scopes ui" },
                { "<c-n>",      "<cmd>Grapple cycle forward<cr>",  desc = "Grapple forward" },
                { "<c-s-n>",    "<cmd>Grapple cycle backward<cr>", desc = "Grapple backward" },
                { "<leader>1",  "<cmd>Grapple select index=1<cr>", desc = "Grapple 1" },
                { "<leader>2",  "<cmd>Grapple select index=2<cr>", desc = "Grapple 2" },
                { "<leader>3",  "<cmd>Grapple select index=3<cr>", desc = "Grapple 3" },
                { "<leader>4",  "<cmd>Grapple select index=4<cr>", desc = "Grapple 4" },
                { "<leader>5",  "<cmd>Grapple select index=5<cr>", desc = "Grapple 5" },
                { "<leader>6",  "<cmd>Grapple select index=6<cr>", desc = "Grapple 6" },
                { "<leader>7",  "<cmd>Grapple select index=7<cr>", desc = "Grapple 7" },
                { "<leader>8",  "<cmd>Grapple select index=8<cr>", desc = "Grapple 8" },
                { "<leader>9",  "<cmd>Grapple select index=9<cr>", desc = "Grapple 9" },
                { "<leader>0",  "<cmd>Grapple select index=0<cr>", desc = "Grapple 0" },
            },
        },

        -- telescope does Fuzy Finding stuf
        {
            "nvim-telescope/telescope.nvim",
            cmd = "Telescope",
            dependencies = {
                "nvim-lua/plenary.nvim",
                "folke/trouble.nvim",
            },
            keys = {
                { "<leader>ff", "<cmd>Telescope find_files<cr>" },
                { "<leader>fg", "<cmd>Telescope git_files<cr>" },
                { "<leader>fb", "<cmd>Telescope git_branches<cr>" },
                { "<leader>fo", "<cmd>Telescope oldfiles<cr>" },
                { "<leader>fr", "<cmd>Telescope lsp_references<cr>" },
                { "<leader>f*", "<cmd>Telescope live_grep<cr>" },
                { "<leader>fb", "<cmd>Telescope buffers<cr>" },
                { "<leader>fa", "<cmd>Telescope commands<cr>" },
                { "<leader>fq", "<cmd>Telescope command_history<cr>" },
                { "<leader>fk", "<cmd>Telescope keymaps<cr>" },
                { "<leader>f?", "<cmd>Telescope current_buffer_fuzzy_find<cr>" },
            },
            config = function ()
                local owt = require('trouble.sources.telescope').open
                require('telescope').setup({
                    defaults = {
                        path_display = { truncate = 3, },
                        mappings = {
                            i = { ["<c-a>"] = owt },
                            n = { ["<c-a>"] = owt },
                        },
                    },
                })
            end
        },

        -- add diagnostics, errors, etc
        {
            "folke/trouble.nvim",
            event = { "VeryLazy" },
            dependencies = { "nvim-tree/nvim-web-devicons" },
            opts = {  }
        },


        -- treesitter for good ast parsing stuff
        -- good tips from https://www.josean.com/posts/nvim-treesitter-and-textobjects
        {
            "nvim-treesitter/nvim-treesitter",
            build = ":TSUpdate",
            event = { "BufReadPre", "BufNewFile" },
            config = function()
                require("nvim-treesitter.install").prefer_get = true
                require("nvim-treesitter.configs").setup({
                    --turn on highlighting and indent control
                    highlight = { enable = true },
                    indent = { enable = true },

                    --install the languages I care about
                    ensure_installed = {
                        -- defaults
                        "vim",
                        "vimdoc",
                        "lua",
                        "luadoc",
                        "query",
                        "diff",

                        -- web dev
                        "html",
                        "css",
                        "javascript",
                        "typescript",
                        "tsx",
                        "svelte",
                        "jsdoc",
                        "http",

                        -- git
                        "gitignore",
                        "gitcommit",
                        "gitattributes",

                        -- data
                        "json",
                        "yaml",
                        "toml",

                        -- other dev
                        "dockerfile",
                        "java",
                        "kotlin",
                        "markdown",
                        "markdown_inline",
                        "rust",
                        "odin",
                        "go",
                        "regex",
                        "cmake",
                        "bash",
                        "comment",
                    },

                    incremental_selection = {
                        enable = true,
                        keymaps = {
                            init_selection = "gnn",
                            node_incremental = "grn",
                            scope_incremental = "grc",
                            node_decremental = "grm",
                        },
                    },
                })
                vim.cmd([[set foldmethod=expr]])
                vim.cmd([[set foldexpr=nvim_treesitter#foldexpr()]])
                vim.cmd([[set nofoldenable]])
            end,
        },
        {
            "nvim-treesitter/nvim-treesitter-textobjects",
            dependencies = { "nvim-treesitter/nvim-treesitter" },
            event = { "BufReadPre", "BufNewFile" },
            config = function()
                require("nvim-treesitter.configs").setup({
                    textobjects = {
                        select = {
                            enable = true,
                            --jump ahead to the next matching object
                            lookahead = true,
                            keymaps = {
                                ["af"] = { query = "@function.outer", desc = "select around a function" },
                                ["if"] = { query = "@function.inner", desc = "select inside a function" },
                                ["ac"] = { query = "@class.outer", desc = "select around a class" },
                                ["ic"] = { query = "@class.inner", desc = "select inside a class" },
                                ["ai"] = { query = "@conditional.outer", desc = "select around a conditional" },
                                ["ii"] = { query = "@conditional.inner", desc = "select inside a conditional" },
                                ["al"] = { query = "@loop.outer", desc = "select around a loop" },
                                ["il"] = { query = "@loop.inner", desc = "select inside a loop" },
                            },
                        },
                        move = {
                            enable = true,
                            set_jumps = true,
                            goto_next_start = {
                                ["]f"] = { query = "@function.outer", desc = "jump to next function start" },
                                ["]c"] = { query = "@class.outer", desc = "jump to next class start" },
                                ["]i"] = { query = "@conditional.outer", desc = "jump to next conditional start" },
                                ["]l"] = { query = "@loop.outer", desc = "jump to next loop start" },
                            },
                            goto_next_end = {
                                ["]F"] = { query = "@function.outer", desc = "jump to next function end" },
                                ["]C"] = { query = "@class.outer", desc = "jump to next class end" },
                                ["]I"] = { query = "@conditional.outer", desc = "jump to next conditional end" },
                                ["]L"] = { query = "@loop.outer", desc = "jump to next loop end" },
                            },
                            goto_previous_start = {
                                ["[f"] = { query = "@function.outer", desc = "jump to previous function start" },
                                ["[c"] = { query = "@class.outer", desc = "jump to previous class start" },
                                ["[i"] = { query = "@conditional.outer", desc = "jump to previous conditional start" },
                                ["[l"] = { query = "@loop.outer", desc = "jump to previous loop start" },
                            },
                            goto_previous_end = {
                                ["[F"] = { query = "@function.outer", desc = "jump to previous function end" },
                                ["[C"] = { query = "@class.outer", desc = "jump to previous class end" },
                                ["[I"] = { query = "@conditional.outer", desc = "jump to previous conditional end" },
                                ["[L"] = { query = "@loop.outer", desc = "jump to next loop end" },
                            },
                        },
                        lsp_interop = {
                            enable = true,
                            border = 'none',
                            floating_preview_opts = {},
                            peek_definition_code = {
                                ["<leader>df"] = "@function.outer",
                                ["<leader>dF"] = "@class.outer",
                            },
                        },
                    }
                })

                local ts_repeat_move = require("nvim-treesitter.textobjects.repeatable_move")
                -- Repeat movement with ; and ,
                -- ensure ; goes forward and , goes backward regardless of the last direction
                vim.keymap.set({ "n", "x", "o" }, ";", ts_repeat_move.repeat_last_move_next)
                vim.keymap.set({ "n", "x", "o" }, ",", ts_repeat_move.repeat_last_move_previous)

                -- make builtin f, F, t, T also repeatable with ; and ,
                vim.keymap.set({ "n", "x", "o" }, "f", ts_repeat_move.builtin_f)
                vim.keymap.set({ "n", "x", "o" }, "F", ts_repeat_move.builtin_F)
                vim.keymap.set({ "n", "x", "o" }, "t", ts_repeat_move.builtin_t)
                vim.keymap.set({ "n", "x", "o" }, "T", ts_repeat_move.builtin_T)
            end

        },

        -- STATUS LINE
        -- status line
        {
            "nvim-lualine/lualine.nvim",
            lazy = false,
            dependencies = { "nvim-tree/nvim-web-devicons", },
            config = function ()
                local function grappledFiles()
                    local taggedFiles = ""
                    for i, obj in ipairs(require('grapple').tags()) do
                        taggedFiles = taggedFiles .. i .. ": " .. vim.fn.fnamemodify(obj.path, ":t") .. " | "
                    end
                    return taggedFiles:gsub(" | $", "")
                end
                local function lsp_clients()
                    local clientNames = ""
                    for _, client in ipairs(vim.lsp.get_clients()) do
                        clientNames = clientNames .. client.name .. " | "
                    end
                    return clientNames:gsub(" | $", "")
                end
                require('lualine').setup({
                    tabline = {
                        lualine_b = {
                            {
                                grappledFiles,
                            }
                        }
                    },
                    sections = {
                        lualine_b = { 'branch', 'diff' },
                        lualine_x = { { 'diagnostics', sources = { 'nvim_lsp', 'nvim_diagnostic' } }, lsp_clients },
                        lualine_y = { 'filesize', 'encoding', 'filetype' },
                    },
                })

            end
        },

        -- undotree for good local history navigation.
        -- note this works across file close due to the opts.undo stuff above
        {
            "mbbill/undotree",
            keys = {
                { "<leader>u", vim.cmd.UndotreeToggle, desc = "Toggle UndoTree" },
            },
        },

        {
            "lewis6991/gitsigns.nvim",
            event = { "VeryLazy" },
            opts = { },
        },


        -- fugitive for :Git commands
        {
            "tpope/vim-fugitive",
            cmd = { "Git" },
        },

        -- LSP / IDE stuff
        -- ===============
        -- Mason to install lsp-servers from within vim
        -- mason-lspconfig to tie lspconfig and mason together
        -- cmp to do autocompletion
        -- lsp-zero to translate between them all
        {
            'VonHeikemen/lsp-zero.nvim',
            branch = 'v3.x',
            --init = function()
            --    vim.g.lsp_zero_extend_cmp = 0
            --    vim.g.lsp_zero_extend_lspconfig = 0
            --end,
            config = function ()
                local lsp_zero = require('lsp-zero')
                lsp_zero.on_attach(function (client, bufnr)
                    -- see :help lsp-zero-keybindings
                    -- to see what this does.
                    lsp_zero.default_keymaps({buffer = bufnr})
                    lsp_zero.set_sign_icons({
                        error = '✘',
                        warn = '▲',
                        hint = '⚑',
                        info = '»'
                    })
                end)
            end
        },
        {
            'williamboman/mason.nvim',
            event = "VeryLazy",
            opts = {},
            build = function()
                vim.cmd('MasonUpdate')
            end,
        },
        {
            'L3MON4D3/LuaSnip',
            dependencies = {
                'rafamadriz/friendly-snippets',
            },
            config = function()
                require('luasnip.loaders.from_vscode').lazy_load()
            end,
        },
        {
            'hrsh7th/nvim-cmp',
            event = 'InsertEnter',
            dependencies = {
                'L3MON4D3/LuaSnip',
                'VonHeikemen/lsp-zero.nvim',
                "rafamadriz/friendly-snippets",
            },
            config = function()
                local cmp = require('cmp')
                local lsp_zero = require('lsp-zero')
                local luasnip = require('luasnip')

                lsp_zero.extend_cmp()
                local cmp_action = lsp_zero.cmp_action()
                local cmp_format = lsp_zero.cmp_format({details = true})

                cmp.setup({
                    window = {
                        completion = cmp.config.window.bordered(),
                        documentation = cmp.config.window.bordered(),
                    },
                    snippet = {
                        expand = function(args)
                            luasnip.lsp_expand(args.body)
                        end,
                    },
                    sourcs = {
                        {name = 'nvim_lsp'},
                        {name = 'buffer'},
                    },
                    formatting = cmp_format,

                    -- add some better keys for choosing autocomplete suggestions
                    mapping = {
                        ["<CR>"] = cmp.mapping({
                            i = function(fallback)
                                if cmp.visible() and cmp.get_active_entry() then
                                    cmp.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false })
                                else
                                    fallback()
                                end
                            end,
                            s = cmp.mapping.confirm({ select = true }),
                            c = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = true }),
                        }),
                        ['<C-Space>'] = cmp.mapping.complete(),
                        ["<Tab>"] = cmp_action.luasnip_supertab(),
                        ["<S-Tab>"] = cmp_action.luasnip_shift_supertab(),
                    }
                })
            end
        },
        {
            'neovim/nvim-lspconfig',
            cmd = {'LspInfo', 'LspInstall', 'LspStart' },
            --event = { "BufReadPre", "BufNewFile" },
            ft = { "lua", "html", "css", "scss", "less", "typescript", "javascript", "markdown", "json", "yaml", "java", "kotlin", "svelte", "rust", "odin", "bash", "python", "go", "zig", "ps1", "psd1", "psm1" },
            dependencies = {
                {'hrsh7th/cmp-nvim-lsp'},
                { 'williamboman/mason-lspconfig.nvim', },
            },
            config = function()
                local lsp_zero = require('lsp-zero').preset({})
                lsp_zero.extend_lspconfig()

                lsp_zero.on_attach(function(_, bufnr)
                    -- see :help lsp-zero-keybindings
                    lsp_zero.default_keymaps({buffer = bufnr, preserve_mappings = false})
                end)


                require('mason-lspconfig').setup({
                    ensure_installed = {
                        "gopls",
                        "ols",
                        "html",
                        "cssls",
                        "ts_ls",
                        "bashls",
                        "jsonls",
                        "marksman",
                        "svelte",
                        "yamlls",
                        "jdtls",
                        "zls",
                        "kotlin_language_server",
                        "powershell_es",
                    },
                    automatic_installation = true,
                    handlers = {
                        lsp_zero.default_setup,
                        -- configure servers here as needed, using lspconfig names
                        jdtls = lsp_zero.noop, --java is configed below via nvim-jdlts
                        yamlls = function()
                            require('lspconfig').yamlls.setup({ yaml = { schemaStore = { enable = true } } } )
                        end,
                        eslint  = function()
                            require('lspconfig').eslint.setup({
                                filetypes = { "javascript", "svelte" }
                            });
                        end,
                        powershell_es = function ()
                            require('lspconfig').powershell_es.setup({
                                bundle_path = '~/.local/share/nvim/mason/packages/powershell-editor-services',
                            })
                        end,
                        lua_ls = function()
                            -- (Optional) Configure lua language server for neovim
                            require('lspconfig').lua_ls.setup(lsp_zero.nvim_lua_ls())
                        end,
                        ts_ls = function ()
                            require('lspconfig').ts_ls.setup({
                                settings = {
                                    completions = {
                                        completeFunctionCalls = true,
                                    },
                                },
                            })
                        end,
                        zls = function()
                            require('lspconfig').zls.setup({
                                cmd =  { "/Users/jshurmer/other-dev/language-servers/zls/zig-out/bin/zls" },
                            })
                        end,
                    },
                })
            end
        },

        {
            "mfussenegger/nvim-jdtls",
            ft = { "java" },
        },

        {
            "norcalli/nvim-colorizer.lua",
            opts = {
                "css", "less", "scss",
            },
            event = { "VeryLazy" }
        },

        {"ellisonleao/glow.nvim", opts = {}, cmd = "Glow"},
        {"mattn/emmet-vim", event = "InsertEnter"},
        { 'stevearc/dressing.nvim', event = "VeryLazy", opts = {} },
    },

    {
        install = { colorschem = { "habamax " } },
        defaults = {
            lazy = true,
        },
        checker = { enabled = true }, -- automatically check for plugin updates
        performance = {
            rtp = {
                -- disable some rtp plugins
                disabled_plugins = {
                    "gzip",
                    -- "matchit",
                    -- "matchparen",
                    -- "netrwPlugin",
                    "tarPlugin",
                    "tohtml",
                    "tutor",
                    "zipPlugin",
                },
            },
        },
    }
)


-- loading this after plenary plugin above
require('aem')
require('bible')
